# backend-monorepo

## Objetivo   
Esse repositório tem como objetivo abarcar todos os repositórios de backend
do projeto Carby. Nele contém 3 serviços distintos indepententes. Eles são:

* Controller: responsável por enviar os comandos para controlar os compoenetes
* Records: responsável por gravar os status de sensores e afins dentro do projeto
* Mocker: por falta da criação da estrutura e da parte eletrônica foi necessário
criar um serviço para simular o envio de mensagem.


## Como executar o projeto   
Para executar o projeto basta ter o docker e o docker-compose instaldo na máquina.

`docker-compose up --build` irá subir todos os serviço e todos os bancos de dados.

* Records: porta 8887
* Controller: porta 8888
* Mocker: porta 8889
