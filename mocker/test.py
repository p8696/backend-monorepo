from client import Client


def subscribe(client):
    def on_message(client, data, msg):
        print(f'Received {msg.payload.decode()} from {msg.topic} topic')

    list_test = [
        ('carby-s-1', 0),
        ('carby-s-2', 0),
        ('carby-s-3', 0),
        ('carby-s-4', 0),
    ]
    try:
        client.subscribe(list_test)
    except Exception as error:
        print(error)
    client.on_message = on_message


if __name__ == "__main__":
    client = Client()
    client.run()
    subscribe(client.client)
    client.client.loop_forever()
