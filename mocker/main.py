from time import sleep
from threading import Thread
from client import Client
from random import uniform
import logging

logging.basicConfig(
    level=logging.INFO
)
logger = logging.getLogger(__name__)


ALL_SENSORS = {
    "sensors": [
        'carby-s-1',
        'carby-s-2',
        'carby-s-3',
        'carby-s-4',
        'carby-mr-1',
        'carby-mr-2'
    ],
    "modules": {
        'carby-m-1': {
            "name": "Filtro",
            "status": "Off"
        },
        'carby-m-2': {
            "name": "Luz UV",
            "status": "Off"
        }
    }
}


def on_message(client, data, msg):
    status = msg.payload.decode()
    topic = msg.topic
    ALL_SENSORS['modules'][topic]['status'] = status
    name = ALL_SENSORS['modules'][topic]['name']
    status = ALL_SENSORS['modules'][topic]['status']
    logger.info(f'Modulo: {name} Status: {status}')


class ProducerMqtt(Thread):
    def __init__(self):
        super(ProducerMqtt, self).__init__()
        self.client = Client()

    def run(self):
        logger.info("Create client producer!")
        self.client.run()
        self.client.client.loop_start()
        logger.info("Init client!")
        while True:
            for each in ALL_SENSORS['sensors']:
                if each in ['carby-mr-1', 'carby-mr-2']:
                    new_topic = each.replace('mr', 'm')
                    data = (
                        '1'
                        if ALL_SENSORS['modules'][new_topic]['status'] == "On"
                        else '0'
                    )
                else:
                    data = f'{round(uniform(20.0, 50.0), 2)}'
                self.client.publish(each, data)
                logger.info(f'Sended {data} to topic: {each}')
            sleep(30)


class ConsumerMqtt(Thread):
    def __init__(self):
        super(ConsumerMqtt, self).__init__()
        self.client = Client()

    def run(self):
        logger.info("Create client consumer!")
        self.client.run(on_message=on_message)
        topics = [(each, 0) for each in ALL_SENSORS['modules'].keys()]
        self.client.subscribe(topics)


if __name__ == "__main__":
    ProducerMqtt().start()
    ConsumerMqtt().start()
