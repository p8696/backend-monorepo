#!/user/bin/env bash                                                             
                                                                                
cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime                         
echo "America/Sao_Paulo" > /etc/timezone                                        
date                                                                            
echo Running                                                                    
exec python3 main.py
