from django.contrib import admin
from .models import (
    Component,
    Record
)

admin.site.register(Component)
admin.site.register(Record)
