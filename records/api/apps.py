import os
import logging
from threading import Thread
from django.apps import AppConfig
import paho.mqtt.client as mqtt


logging.basicConfig(
    level=logging.INFO
)
logger = logging.getLogger(__name__)


class MqttClient(Thread):
    def __init__(self, broker, port, timeout):
        super(MqttClient, self).__init__()
        self.client = mqtt.Client()
        self.broker = broker
        self.port = port
        self.timeout = timeout

    @property
    def topics(self) -> list:
        from .models import Component
        topics = list(
            Component.objects.filter().values_list('topic', flat=True)
        )
        topics = [(each, 0) for each in topics]
        return topics

    def on_message(self, client, userdata, msg):
        from .models import (
            Component,
            Record
        )
        logger.info(
            f"Receive message: {msg.payload.decode()} from {msg.topic}"
        )
        component = Component.objects.filter(topic=msg.topic).last()
        Record.objects.create(
            component_id=component,
            value=float(msg.payload.decode())
        )
        logger.info('Record saved!')

    def on_connect(self, client, userdata, flags, rc):
        topics = self.topics
        if topics == []:
            logger.info('Without topics to subscribe')
        else:
            self.client.subscribe(topics)

    def connect_to_broker(self):
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.connect(self.broker, self.port, self.timeout)
        self.client.loop_forever()

    def run(self):
        self.connect_to_broker()


class ApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api'

    def ready(self):
        MqttClient(
            os.environ.get('BROKER'), int(os.environ.get('BROKER_PORT')), 60
        ).start()
