import os
from django.views.generic import View
from .models import Component, Record
from django.http import JsonResponse
from datetime import datetime, timedelta


def auth_per(request):
    if request.headers.get('token') != os.environ.get('TOKEN', "teste"):
        return JsonResponse(
            {"error": "Permission denied!"}
        )
    return None


class AddComponentView(View):
    def post(self, request):
        auth = auth_per(request)
        if auth is not None:
            return auth
        print(request.POST)
        if 'name' not in request.POST or 'topic' not in request.POST:
            return JsonResponse({"error": "Missing field name or topic"})
        try:
            Component.objects.create(
                name=request.POST.get('name'),
                topic=request.POST.get('topic')
            )
            return JsonResponse(
                {"message": f"create {request.POST['name']} with sucess"}
            )
        except Exception as error:
            return JsonResponse(
                {
                 "error": f"{error}"
                }
            )


class RecordDetailView(View):
    def post(self, request):
        print(request.POST)
        auth = auth_per(request)
        if auth is not None:
            return auth
        try:
            name = request.POST['name']
        except KeyError:
            return JsonResponse(
                {'error': 'Missing field name!'}
            )
        component = Component.objects.filter(name=name).last()
        list_values = list(component.record_set.filter(
            date__range=(
                datetime.now() - timedelta(minutes=30), datetime.now()
            )
        ).values(
                'value', 'date'
            )
        )
        return JsonResponse(
            {'data': list_values}
        )


class AllRecordsView(View):
    def get(self, request):
        auth = auth_per(request)
        if auth is not None:
            return auth
        response = list(Record.objects.filter(
            date__range=(
                datetime.now() - timedelta(minutes=30), datetime.now()
            )
        ).values('value', 'date', 'component_id__name'))
        return JsonResponse(
            {'data': response}
        )
