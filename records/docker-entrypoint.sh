#!/user/bin/env bash                                                             
                                                                                
cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime                         
echo "America/Sao_Paulo" > /etc/timezone                                        
date                                                                            
exec python manage.py makemigrations &                                          
exec python manage.py migrate&                                                  
# exec ./manage.py runserver 0.0.0.0:8000                                       
echo Running                                                                    
exec gunicorn $APP_1.wsgi -b 0.0.0.0:$APP_1_PORT --log-file -
