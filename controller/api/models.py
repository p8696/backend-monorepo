from django.db import models


class ComponentController(models.Model):
    topic = models.CharField(max_length=255, primary_key=True)
    name = models.CharField(max_length=255, unique=True)
    read = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.name}"


class RecordController(models.Model):
    value = models.FloatField()
    component_id = models.ForeignKey(
        ComponentController,
        on_delete=models.CASCADE
    )
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.component_id} | {self.value} | {self.date}"
