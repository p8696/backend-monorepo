import os
from django.views.generic import View
from .models import ComponentController, RecordController
from django.http import JsonResponse
from .services import Client


def auth_per(request):
    if request.headers.get('token') != os.environ.get('TOKEN', "teste"):
        return JsonResponse(
            {"error": "Permission denied!"}
        )
    return None


class AddComponentView(View):
    def post(self, request):
        auth = auth_per(request)
        if auth is not None:
            return auth
        print(request.POST)
        if 'name' not in request.POST or 'topic' not in request.POST:
            return JsonResponse({"error": "Missing field name or topic"})
        try:
            ComponentController.objects.create(
                name=request.POST.get('name'),
                topic=request.POST.get('topic')
            )
            return JsonResponse(
                {"message": f"create {request.POST['name']} with sucess"}
            )
        except Exception as error:
            return JsonResponse(
                {
                 "error": f"{error}"
                }
            )


class RecordDetailView(View):
    def post(self, request):
        print(request.POST)
        auth = auth_per(request)
        if auth is not None:
            return auth
        try:
            name = request.POST['name']
        except KeyError:
            return JsonResponse(
                {'error': 'Missing field name!'}
            )
        component = ComponentController.objects.filter(name=name).last()
        values = component.recordcontroller_set.filter().last()
        data = {
            "name": component.name,
            "value": values.value,
            "date": values.date
        }
        return JsonResponse(
            {'data': data}
        )


class AllRecordsView(View):
    def get(self, request):
        auth = auth_per(request)
        if auth is not None:
            return auth
        component = ComponentController.objects.all()
        print(component)
        response = []
        for each in component:
            data = each.recordcontroller_set.last()
            response.append(
                {
                    "name": each.name,
                    "value": data.value,
                    "date": data.date
                }
            )
        return JsonResponse(
            {'data': response}
        )


class ChangeComponentStatusView(View):
    def post(self, request):
        auth = auth_per(request)
        if auth is not None:
            return auth
        if 'name' not in request.POST or 'status' not in request.POST:
            return JsonResponse({"error": "Missing field name or status"})
        component = ComponentController.objects.filter(
            name=request.POST['name']
        ).last()
        topic = component.topic
        topic = topic.replace('mr', 'm')
        try:
            client = Client()
            client.run()
            client.client.loop_start()
            client.publish(topic, request.POST['status'])
            client.client.loop_stop()
        except Exception as error:
            return JsonResponse({
                "error": f"{error}"
            })
        return JsonResponse(
            {
                "message": f"Module: {request.POST['name']} success: {request.POST['status']}" # noqa
            }
        )
