from django.conf.urls import url
from .views import (
    AddComponentView,
    RecordDetailView,
    AllRecordsView,
    ChangeComponentStatusView
)


urlpatterns = [
    url(r'^component/add/', AddComponentView.as_view(), name="add-component"),
    url(r'^record/detail/', RecordDetailView.as_view(), name="detail-record"),
    url(r'^record/all/', AllRecordsView.as_view(), name="all-records"),
    url(
        r'^component/status/',
        ChangeComponentStatusView.as_view(),
        name="change-status")
]
