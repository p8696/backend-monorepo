import logging
from paho.mqtt import client as mqtt

logging.basicConfig(
    level=logging.INFO
)
logger = logging.getLogger(__name__)


class Client:
    def __init__(self, port=1883, timeout=60):
        self.broker_url = "mqtt.eclipseprojects.io"
        self.port = 1883
        self.timeout = 60
        self.client = None

    def on_connect(self, client, data, flags, rc):
        logger.info(f"Connect with return code: {rc}")

    def on_message(self, client, data, msg):
        logger.info(f"Topic: {msg.topic} DATA: {msg.payload}")

    def publish(self, topic, msg):
        if self.client is None:
            raise Exception("Try publish without connection!")
        try:
            response = self.client.publish(topic, msg)
            if response[0] == 0:
                logger.info(f'Message: {msg} send to topic: {topic}!')
            else:
                logger.info(f'Faild to send {msg} to topic: {topic}!')
        except Exception as error:
            raise error

    def subscribe(self, topics):
        if topics == []:
            logger.info('Without topics')
        else:
            self.client.subscribe(topics)
            self.client.loop_forever()

    def run(self, on_message=None):
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        if on_message is None:
            self.client.on_message = self.on_message
        else:
            self.client.on_message = on_message

        self.client.connect(self.broker_url, self.port, self.timeout)
