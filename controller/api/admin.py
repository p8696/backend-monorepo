from django.contrib import admin
from .models import (
    ComponentController,
    RecordController
)

admin.site.register(ComponentController)
admin.site.register(RecordController)
